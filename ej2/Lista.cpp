#include <iostream>
using namespace std;

#include "Nodo.h"
#include "Lista.h"

Lista::Lista() {}

void Lista::crear (int numero) {
    //Se crea un nodo temporal que apunta a raiz y uno vacio//
    Nodo *tmp= new Nodo;
	Nodo *nod = this->raiz;
    /* asigna la instancia de Numero. */
    tmp->numero = numero;
    /* apunta a NULL por defecto. */
    tmp->sig = NULL;

    /* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    /* de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo como el último de la lista. */
    } else {
		//si no es el primer nodo, se abre un ciclo para definir en que posicion de la lista debe ser insertado//
		while (nod != NULL) {
			//si es igual o menor al primer nodo de la lista, lo reemplaza como la raiz//
			if(numero <= this->raiz->numero){
				tmp->sig = this->raiz;
				this->raiz = tmp;
				//Y se rompe el ciclo//
				break;
			}else{
				//si es mayor que el primer nodo de la lista, se pregunta si nod es el ultimo nodo de la lista// 
				if(nod->sig != NULL){
					//se pregunta si numero es menor que el numero siguiente//
					if(numero < nod->sig->numero){
						//si es, el puntero siguiente de tmp se iguala al puntero siguiente de nod//
						tmp->sig = nod->sig;
						//tmp se iguala al puntero siguiente de nod//
						nod->sig = tmp;
						//y nod se reemplaza con tmp//
						nod = tmp;
						//finalmente se rompe el ciclo//
						break;
					}else{
						//si no es menor, nod se iguala al siguiente nodo de la lista y se continua el ciclo//
						nod = nod->sig;
					}
				}else{
					//si nod es el ultimo nodo de la lista tmp reemplaza al ultimo nodo de la lista//
					this->ultimo->sig = tmp;
					this->ultimo = tmp;
					break;
				}
			}
		}
    }
}

void Lista::imprimir () {  
    /* utiliza variable temporal para recorrer la lista. */
    Nodo *tmp = this->raiz;
    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    while (tmp != NULL) {
        cout << tmp->numero <<" ";
        tmp = tmp->sig;
    }
    cout<< endl;
}
