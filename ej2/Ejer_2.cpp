#include "Nodo.h"
#include "Lista.h"
#include "Ejer_2.h"
#include <string>
#include <iostream>
using namespace std;

int main (void) {
	//Se crea el objeto tipo Ejercicio_2 para correr el programa//
	Ejercicio_2 *ejerc = new Ejercicio_2();
    //Se llama a la funcion run de Ejercicio_2//
	//run() contiene las instrucciones necesarias para funcionar//
    ejerc->run();
	//Se retorna 0 para acabar el main//
	
    return 0;
}
