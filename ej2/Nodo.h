#include <iostream>
#ifndef NODO_H
#define NODO_H

/* define la estructura del nodo. */
typedef struct _Nodo {
	//Se define con una variable tipo int y una tipo nodo// 
    int numero;
    struct _Nodo *sig;
} Nodo;

#endif
