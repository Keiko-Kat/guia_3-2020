#include "Nodo.h"
#include "Lista.h"
using namespace std;



class Ejercicio {
    private:
		//Se crea con tres caracteristicas tipo lista//
        Lista *lista1 = NULL;
        Lista *lista2 = NULL;
        Lista *lista3 = NULL;

    public:
        //Se crea un constructor//
        Ejercicio() {
            this->lista1 = new Lista();
            this->lista2 = new Lista();
            this->lista3 = new Lista();
        }
        
        //Y tres funciones que retornen una lista cada una//
        Lista *get_lista_1() {
            return this->lista1;
        }
        Lista *get_lista_2() {
            return this->lista2;
        }
        Lista *get_lista_3() {
            return this->lista3;
        }
};

class Ejercicio_2{
		
	public:
	
		//catch_string se asegura de evitar un error de conversion//
		//Asegurando que solo existan numeros en el string ingresado//
		string catch_string(string in){
			//Esta funcion recibe un string ingresado por teclado en la funcion principal//
			//para validar la entrada de un int primero se crea y define is_true como verdadero por default//
			bool is_true = true;
			//Se abre un ciclo while que corre mientras is_true sea verdadero//
			while (is_true == true){
				//Dentro del while se abre un ciclo for que analiza el string recibido//
				for(int j = 0; j < in.size(); j++){
					//Se recorre y se comparan los valores ASCII contenidos en el string//
					//A los valores ASCII de los numros del 0-9 y al valor de '-'//
					if((in[j] >= 48 && in[j] <= 57) || in[j] == 45){
						//si el contenido del string no coincide con un valor ASCII numerico o de "-" is_true se iguala a falso//
						is_true = false;
					}else{
						//Si el contenido del string si coincide, is_true se iguala a verdadero y se rompre el ciclo//
						is_true = true;
						break;
					}
				}
				//Luego si se encuentra algo que no es un numero o '-' en el string ingresado//
				if(is_true == true){
					//Se entrega un mensaje de error//
					cout<<"Por favor ingrese valores numéricos:"<<endl;
					//Y se pide otro ingreso//
					getline(cin, in);
				}
			}
			//De esta forma se asegura que la funcion 'stoi()' no entregue error//
			return in;
		}
		
		int menu(){
			//Se define una variable string que recibe un ingreso de teclado//
			//Y una variable int que recibe el valor y lo retorna a la funcion principal//
			string in;
			int temp;
			cout<<endl;
			//Luego se imprimen las opciones que se le dan al usuario en pantalla//
			cout<<"Agregar lista_1          [1]"<<endl;
			cout<<"Agregar lista_2          [2]"<<endl;
			cout<<"Ver Lista final          [3]"<<endl;
			cout<<"Cerrar programa          [0]"<<endl;
			cout<<"----------------------------"<<endl;
			cout<<"----------------------------"<<endl;
			cout<<"Opción:  ";
			//Para luego pedir que ingrese la opcion que desea realizar//
			//Se recibe como ingreso de teclado//
			getline(cin, in);
			//Y se comprueba que sea un valor completamene numerico//
			in = catch_string(in);
			//Para luego transformarlo a una variable int//		
			temp = stoi(in);
			//Y retornar dicho int como eleccion a la funcion principal//
			return temp;
		}	
		
		//run contiene todas las instrucciones para correr el programa//
		void run(){			
			//Se crea una variable tipo ejercicio//	
			Ejercicio e = Ejercicio();
			//De esta variable, cada lista se recupera en una variable externa//
			Lista *lista_1 = e.get_lista_1();
			Lista *lista_2 = e.get_lista_2();
			Lista *lista_3 = e.get_lista_3();
			//se crea un avariable string que reciba los ingresos de teclado//
			string in;
			//y dos variables int para correr el programa//
			int num, temp = -1;
			
			//se abre un ciclo en el cual se corre el programa//
			while(temp != 0){
				//Se llama a la funcion menu de ejercicio_2 y el resultado se guarda en temp//
				temp = menu();
				//Se abre un switch dependiente de temp//
				switch(temp){
					//si temp == 1 se pide al usuario que ingrese un numero a la lista 1//
					case 1: cout<<"Ingrese un numero entero:"<<endl;
							cout<<"   ~~~> ";
							//Se recibe como ingreso de teclado//
							getline(cin, in);
							//Y se comprueba que sea un valor completamene numerico//
							in = catch_string(in);
							//Para luego transformarlo a una variable int//
							num = stoi(in);
							//Se imprime el inicio de la lista//
							cout<<"Lista 1: ";
							//Se inserta num tanto en la primera como en la tercera lista//
							lista_1->crear(num); 
							lista_3->crear(num);      
							//Se imprimen los contenidos de la primera lista//
							lista_1->imprimir();
							//Y se rompe el ciclo//
							break;
					//si temp == 2 se pide al usuario que ingrese un numero a la lista 2//					
					case 2: cout<<"Ingrese un numero entero:"<<endl;
							cout<<"   ~~~> ";
							//Se recibe como ingreso de teclado//
							getline(cin, in);
							//Y se comprueba que sea un valor completamene numerico//
							in = catch_string(in);
							//Para luego transformarlo a una variable int//
							num = stoi(in);
							//Se imprime el inicio de la lista//
							cout<<"Lista 2: ";
							//Se inserta num tanto en la segunda como en la tercera lista//
							lista_2->crear(num); 
							lista_3->crear(num); 
							//Se imprimen los contenidos de la segunda lista//     
							lista_2->imprimir();
							//Y se rompe el ciclo//
							break;
					//si temp == 3 se imprimen los contenidos de las tres listas//
					case 3: cout<<"Lista 1: ";
							lista_1->imprimir();
							cout<<"Lista 2: ";
							lista_2->imprimir();
							cout<<"Lista 3: ";
							lista_3->imprimir();
							temp = 0;
							//se vuelve temp=0 para detener el while//
							//y se rompe el ciclo//
							break;
					//si temp == 0, se rompe el ciclo while y se cierra el programa//
					case 0: break;
					//Si temp no es una de las opcoones anteriores, entrega un mensaje de error y se reinicia el while//
					default: cout<<"Opcion invalida, intente nuevamente"<<endl;
				}
			}
		}
};
