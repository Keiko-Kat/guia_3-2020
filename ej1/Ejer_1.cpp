#include "Nodo.h"
#include "Lista.h"
#include "Ejer_1.h"
#include <string>
#include <iostream>
using namespace std;

int main (void) {
	//Se crea el objeto tipo Ejercicio_1 para correr el programa//
	Ejercicio_1 *ejerc = new Ejercicio_1();
    //Se llama a la funcion run de Ejercicio_1//
	//run() contiene las instrucciones necesarias para funcionar//
    ejerc->run();
	//Se retorna 0 para acabar el main//
	
    return 0;
}
