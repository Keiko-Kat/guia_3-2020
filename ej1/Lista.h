#include <iostream>
#include "Nodo.h"
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
    //Se definen el primer y ultimo nodo, como referencias en la lista//
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        /* constructor*/
        Lista();
        
        /* crea un nuevo nodo, recibe una instancia tipo int */
        void crear (int numero);
        /* imprime la lista. */
        void imprimir ();
};
#endif
