# Guia_3-2020

Los ejercicios fueron resueltos en Geany, ejecutados desde terminal para testeo en linux 4.9.0-12-amd64
 

# ej_1

Este ejercicio requeria solo el almacenamiento de una lista ordenada

El requerimiento fue que los numeros fueran ingresados por el usuario


Abrir terminal en la carpeta ej1

Ejecutar comando make por terminal

Ejecutar comando ./ejer_1

Seguir instrucciones mostradas por pantalla



# ej_2

Este ejercicio requeria el almacenamiento de tres listas ordenadas

Dos que recibieran ingresos de usuario y una que uniera esas dos

Abrir terminal en la carpeta ej2

Ejecutar comando make por terminal

Ejecutar comando ./ejer_2

Seguir instrucciones mostradas por pantalla

# ej_3

Este ejercicio requeria solo el almacenamiento de una lista ordenada

Que recibiera ingresos desde terminal, y que de ser numeros no-consecutivos


Que llenara los "espacios en blanco" de la lista

Abrir terminal en la carpeta ej3

Ejecutar comando make por terminal

Ejecutar comando ./ejer_3

Seguir instrucciones mostradas por pantalla

# general

El mayor problema fue como insertar nodos al "interior" de una lista

Lo que resolvi con un poco de prueba y error al intentar asignar distintos 

Valores a los nodos

