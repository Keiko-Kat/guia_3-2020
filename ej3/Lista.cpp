#include <iostream>
using namespace std;

#include "Nodo.h"
#include "Lista.h"

Lista::Lista() {}

void Lista::crear (int numero) {
    //Se crea un nodo temporal que apunta a raiz y uno vacio//
    Nodo *tmp= new Nodo;
	Nodo *nod = this->raiz;
    /* asigna la instancia de Numero. */
    tmp->numero = numero;
    /* apunta a NULL por defecto. */
    tmp->sig = NULL;

    /* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    } else {
		//si no es el primer nodo, se abre un ciclo para definir en que posicion de la lista debe ser insertado//
		while (nod != NULL) {
			//si es igual o menor al primer nodo de la lista, lo reemplaza como la raiz//
			if(numero <= this->raiz->numero){
				tmp->sig = this->raiz;
				this->raiz = tmp;
				//Y se rompe el ciclo//
				break;
			}else{
				//si es mayor que el primer nodo de la lista, se pregunta si nod es el ultimo nodo de la lista// 
				if(nod->sig != NULL){
					//se pregunta si numero es menor que el numero siguiente//
					if(numero < nod->sig->numero){
						//si es, el puntero siguiente de tmp se iguala al puntero siguiente de nod//
						tmp->sig = nod->sig;
						//tmp se iguala al puntero siguiente de nod//
						nod->sig = tmp;
						//y nod se reemplaza con tmp//
						nod = tmp;
						//finalmente se rompe el ciclo//
						break;
					}else{
						//si no es menor, nod se iguala al siguiente nodo de la lista y se continua el ciclo//
						nod = nod->sig;
					}
				}else{
					//si nod es el ultimo nodo de la lista tmp reemplaza al ultimo nodo de la lista//
					this->ultimo->sig = tmp;
					this->ultimo = tmp;
					break;
				}
			}
		}
    }
}

void Lista::filling_finale (){
	//Se crea un nodo temporal que apunta a raiz//
	Nodo *nod = this->raiz;
	//Se abre un ciclo para llenar la lista//
    while (nod != NULL) {
		//si la lista solo contiene un elemento, se corta el ciclo inmediatamente//
		if(this->raiz == this->ultimo){
			break;
		}
		//Se crea un int igual al contenido numero del nodo a "estudiar"//
		int test = nod->numero;
		// crea un nodo //
		Nodo *tmp;
		tmp = new Nodo;
		// se asigna como 0 //
		tmp->numero = 0;
		// apunta a NULL por defecto. //
		tmp->sig = NULL;
		//Si el numero de nod es menor al antecesor del numero del nodo que le sigue//
		if(nod->numero < nod->sig->numero - 1){
			//numero de tmp, se iguala al sucesor de temp//
			tmp->numero = test+1;
			//el puntero siguiente de tmp se iguala al puntero siguiente de nod//
			tmp->sig = nod->sig;
			//tmp se iguala al puntero siguiente de nod//
			nod->sig = tmp;
			//y nod se reemplaza con tmp//
			nod = tmp;
		}else{
			//Si no lo es, nod se iguala a su sucesor//
			nod = nod->sig;
		}	
		//Se revisa si nod es el antecesor del ultimo nodo//
		if(nod->numero == this->ultimo->numero - 1){
			//si lo es, el ciclo se rompe//
			break;
		}
	}
	//y para terminar, se imprimen los contenidos de la lista//
	imprimir();
}

void Lista::imprimir () {  
    /* utiliza variable temporal para recorrer la lista. */
    Nodo *tmp = this->raiz;
	cout<<"Numero: ";
    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    while (tmp != NULL) {
        cout << tmp->numero <<" ";
        tmp = tmp->sig;
    }
    cout<< endl;
}
