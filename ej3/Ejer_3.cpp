#include "Nodo.h"
#include "Lista.h"
#include "Ejer_3.h"
#include <string>
#include <iostream>
using namespace std;

int main (void) {
	//Se crea el objeto tipo Ejercicio_3 para correr el programa//
	Ejercicio_3 *ejerc = new Ejercicio_3();
    //Se llama a la funcion run de Ejercicio_3//
	//run() contiene las instrucciones necesarias para funcionar//
    ejerc->run();
	//Se retorna 0 para acabar el main//
	
    return 0;
}
