#include "Nodo.h"
#include "Lista.h"
using namespace std;



class Ejercicio {
    private:
		//Se crea con una caracteristica tipo lista//
        Lista *lista = NULL;

    public:
        //Se crea un constructor//
        Ejercicio() {
            this->lista = new Lista();
        }
        //Y una funcion que retorne lista//
        Lista *get_lista() {
            return this->lista;
        }
};

class Ejercicio_3{
		
	public:
	
		//catch_string se asegura de evitar un error de conversion//
		//Asegurando que solo existan numeros en el string ingresado//
		string catch_string(string in){
			//Esta funcion recibe un string ingresado por teclado en la funcion principal//
			//para validar la entrada de un int primero se crea y define is_true como verdadero por default//
			bool is_true = true;
			//Se abre un ciclo while que corre mientras is_true sea verdadero//
			while (is_true == true){
				//Dentro del while se abre un ciclo for que analiza el string recibido//
				for(int j = 0; j < in.size(); j++){
					//Se recorre y se comparan los valores ASCII contenidos en el string//
					//A los valores ASCII de los numros del 0-9 y al valor de '-'//
					if((in[j] >= 48 && in[j] <= 57) || in[j] == 45){
						//si el contenido del string no coincide con un valor ASCII numerico o de "-" is_true se iguala a falso//
						is_true = false;
					}else{
						//Si el contenido del string si coincide, is_true se iguala a verdadero y se rompre el ciclo//
						is_true = true;
						break;
					}
				}
				//Luego si se encuentra algo que no es un numero o '-' en el string ingresado//
				if(is_true == true){
					//Se entrega un mensaje de error//
					cout<<"Por favor ingrese valores numéricos:"<<endl;
					//Y se pide otro ingreso//
					getline(cin, in);
				}
			}
			//De esta forma se asegura que la funcion 'stoi()' no entregue error//
			return in;
		}
		
		//run contiene todas las instrucciones para correr el programa//
		void run(){		
			//Se crea una variable tipo ejercicio//	
			Ejercicio e = Ejercicio();
			//De esta variable, se recupera la lista en una variable externa//
			Lista *lista = e.get_lista();
			//se crea un avariable string que reciba los ingresos de teclado//
			string in;
			//y dos variables int para correr el programa//
			int num, temp = 0;
			//se abre un ciclo en el cual se corre el programa//
			while(temp == 0){
				//se le pide al usuario que entregue un numero por teclado//
				cout<<"Ingrese un numero entero:"<<endl;
				cout<<"   ~~~> ";
				//Se recibe como ingreso de teclado//
				getline(cin, in);
				//Y se comprueba que sea un valor completamene numerico//
				in = catch_string(in);
				//Para luego transformarlo a una variable int//	
				num = stoi(in);
				
				//se llama la funcion crear de lista con el ingreso num//
				lista->crear(num);    
				//y se imprime el contenido de la lista//
				lista->imprimir();
				
				//luego se le pide al usuario que decida entre ingresar un nuevo numero o cerrar el programa//
				cout<<"Para ingresar otro numero ingrese 0"<<endl;
				cout<<"Para salir ingrese cualquier numero"<<endl;
				cout<<"   ~~~> ";
				//Se recibe como ingreso de teclado//
				getline(cin, in);
				//Y se comprueba que sea un valor completamene numerico//
				in = catch_string(in);
				//Para luego transformarlo a una variable int//	
				temp = stoi(in);
				
			}
			//Finalmente se llama a filling_finale para llenar los posibles espacios que queden dentro de la lista//
			lista->filling_finale();
		}
};
